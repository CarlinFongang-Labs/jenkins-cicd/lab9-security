# Jenkins | Security


------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Context
1. Install the `Role-based Authorization Strategy` plugin.
2. Create a bastion user.
3. Create a role named "viewer" with the only right being the ability to view the project.
4. Assign this role to the user "eazytraining".
5. Log in as the bastion user to verify that they have access to the resources within the limits of the rights assigned to them.

## Requirements
1. Set up a CI/CD pipeline with Jenkins. [Documentation](https://gitlab.com/CarlinFongang-Labs/jenkins-cicd/lab5-deploiement)

2. Install plugins: Slack notification, Github integration.

## User Authorization Configuration
### Installing the Role-based Plugin
Dashboard > Manage Jenkins > Plugins > Available Plugins 
>![alt text](img/image.png)
*Click on Install*

### Creating a New User
Dashboard > Manage Jenkins > Users > + Create User
>![alt text](img/image-1.png)
>![alt text](img/image-2.png)
*List of users*

### Associating Permissions with the Role-based Authorization Strategy Plugin
Dashboard > Manage Jenkins > Security > Authorization
Select "Role-Based Strategy"

>![alt text](img/image-3.png)
*Selecting "Role-Based Strategy"*

### Role Configuration
1. Access the role configuration table
Dashboard > Manage Jenkins > Manage and Assign Roles > Manage Roles
>![alt text](img/image-4.png)

2. Fill in the fields in the "Project Roles" section and click on "Add"
`viewer`, `test*`
>![alt text](img/image-5.png)

3. Adding "viewer" to Global Roles
Add viewer and save
>![alt text](img/image-8.png)
*Assign and save*

4. Assigning Permissions to the Role
Once the permissions are checked, save to apply the changes
>![alt text](img/image-6.png)
*Assigning permissions to the test role*

5. In Assign Roles, add bastion to Global Roles and Project Roles
Click on Add User to add the user bastion, check the corresponding boxes for assignments, and validate
>![alt text](img/image-7.png)

### Connecting as the 'bastion' User for Verification
>![alt text](img/image-9.png)
*Login Interface*

1. It can be observed that the only accessible pipeline for 'bastion' is indeed 'Test-pipeline'
>![alt text](img/image-10.png)

2. Limited Actions for the 'bastion' User
Build, log, and other functionalities are unavailable for this user.
>![alt text](img/image-12.png)

3. 'Carlinfg', being an admin, can access all pipelines.
>![alt text](img/image-11.png)









